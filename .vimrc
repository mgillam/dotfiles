" Install vim-plug if not already done
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Load plugins
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'pangloss/vim-javascript'
" JSX syntax
Plug 'mxw/vim-jsx'
" Svelte syntax
Plug 'burner/vim-svelte'
" Node QOL stuff
Plug 'moll/vim-node'
" Emmet 
Plug 'mattn/emmet-vim'
" ALE linter integration
Plug 'w0rp/ale'
" ES2015 code snippets (Optional)
Plug 'epilande/vim-es2015-snippets'
" React code snippets
Plug 'epilande/vim-react-snippets'
" Ultisnips
Plug 'SirVer/ultisnips'
call plug#end()

" Configure emmet
let g:user_emmet_leader_key=','
let g:user_emmet_settings = {
  \  'javascript.jsx' : {
    \      'extends' : 'jsx',
    \  },
  \}

" Configure ultisnips
let g:UltiSnipsExpandTrigger="<tab><tab>"
"
" Configure Ale
let g:ale_sign_error = '●'
let g:ale_sign_warning = '.'
let g:ale_lint_on_enter = 0

" Always do syntax highlighting
syntax on

" Relative number, hybrid mode for navigating
set number relativenumber

" Switch to absolute line number when in insert mode
""augroup numbertoggle
""  autocmd!
""  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
""  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
""augroup END

" Always show status bar with powerline
set laststatus=2
set rtp+=/Users/mic/Library/Python/2.7/lib/python/site-packages/powerline/bindings/vim
